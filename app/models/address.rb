class Address < ApplicationRecord
	validates :pincode, presence: true, length: { in: 6..6 }
	validates :pincode, format: { with: /\A\d+\z/, message: "Only integers allowed." }
	# validates :phone_no,:presence => true
  validates :phone_no, :numericality => true,
                 :length => { :minimum => 10, :maximum => 10 },
                 format: { with: /\A\d+\z/, message: "Only integers allowed." }

	belongs_to :user, optional: true 

	def full_address
		"#{self.street}, #{self.city}, #{self.state}, #{self.landmark}" 
	end
end
