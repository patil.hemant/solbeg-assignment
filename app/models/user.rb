class User < ApplicationRecord
 	mount_uploader :profile_picture, AttachmentUploader
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :name, presence: true, length: { in: 6..25 }

  has_one :address, dependent: :destroy
  accepts_nested_attributes_for :address, :allow_destroy => true

  after_create :set_role
  after_create :set_profile_pic

  def set_profile_pic
    dest_folder = FileUtils.mkdir_p "#{Rails.root}/public/uploads/#{self.type.underscore}/profile_picture/#{self.id.to_s}"
    FileUtils.cp(self.profile_picture.path, dest_folder.last)
  end

  def set_role
    self.update_column('type', 'RegularUser') if self.type.nil?
  end

  def admin?
		type == "Admin"
	end

	def regular_user?
		type == "RegularUser"
	end 

end
