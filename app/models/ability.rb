# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:

    user ||= User.new # guest user (not logged in)
    if user.admin?
      # can :update_only_phone_no, user_id: user.id
      # can_touch_attributes Address, [:phone_no], :user_id=> user.id
      # can? :update, Address, :phone_no, User
      # can :update_course, User
      can :index, User
      can :manage, User
    end

    if user.regular_user?
      cannot :index, User
      can :manage, User, id: user.id
    end

  end
end
