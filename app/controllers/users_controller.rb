class UsersController < ApplicationController
  before_action :check_self_access

  def edit
      @user = User.find(params[:id])
  end

  def update
      @user = User.find(params[:id])
      if @user.update_attributes(user_params)
          redirect_to root_path, notice: "Updated User."
      else
          render :edit
      end
  end

	private

	def user_params
	  params.require(:regular_user).permit(:email, :password, :password_confirmation, :name, :profile_picture ,:address_attributes =>[:address_line, :street, :city, :state, :landmark, :pincode, :phone_no] )
	end
end




