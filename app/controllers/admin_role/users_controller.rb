class AdminRole::UsersController < ApplicationController
	
  before_action :check_admin
  before_action :set_user, only: [:edit, :update]
  before_action :set_flag, only: [:edit, :update]

	def index
		@users = User.all.order('created_at DESC').includes(:address)#.paginate(page: params[:page], per_page: 10)
	end

	def edit
  end

  def update
    if @user.update_attributes(user_params)
        redirect_to admin_role_users_path, notice: "Updated User."
    else
        render :edit
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def set_flag
    @flag = (current_user.admin? and (current_user == @user ))  
  end

	def user_params
    if current_user.admin? and current_user == User.find(params[:id])
      params.require(:admin).permit(:id, :address_attributes =>[:id, :phone_no] )
    else
	     params.require(:regular_user).permit(:id, :email, :password, :password_confirmation, :name, :profile_picture ,:address_attributes =>[:id, :address_line, :street, :city, :state, :landmark, :pincode, :phone_no] )
    end
	end

end
