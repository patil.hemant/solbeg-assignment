class HomeController < ApplicationController
  def index
  	if user_signed_in?
  		@user = current_user 
  		@address = @user.address
  	end
  end

  def cities  	
  	cities = CS.cities(params[:state], :in)
    respond_to do |format|
      format.json { render :json => cities }
    end
  end
end
