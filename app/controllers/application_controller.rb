class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  

  def after_sign_in_path_for(resource)
  	if resource.admin?
  		admin_role_users_path
  	else
  		root_path
  	end
	end

  def check_admin
    return redirect_to root_path unless current_user.admin? 
  end

  def check_self_access
    return redirect_to root_path unless current_user == User.find(params[:id]) 
  end

  protected

  def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:name, :email, :password, :profile_picture, :password_confirmation, :address_attributes => [:address_line, :street, :landmark, :city, :state, :pincode, :phone_no]) }
  end
end
