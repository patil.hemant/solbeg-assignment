Rails.application.routes.draw do
  root  'home#index'
  get 'home/index'
  get "/home/:state/cities"=> "home#cities"
  devise_for :users
  resources :profile, :controller => 'users'
  namespace :admin_role do
	  resources :users
	end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
