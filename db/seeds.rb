# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.destroy_all
india_states = CS.states(:in).keys
admin = User.create(name: 'Solbeg Admin', email: 'admin@gmail.com', password: 'password', type: 'Admin', profile_picture: File.open("#{Rails.root}/app/assets/images/#{['profile1.png','profile2.png', 'profile3.jpeg'].sample}"))
admin.build_address(
											address_line: Faker::Address.street_address,
											street: Faker::Address.street_name,
											landmark: "Near " + Faker::Bank.name + " Bank",
											city: 'Airoli',
											state: 'mh',
											pincode: "421201"
										).save
		

30.times {

						u = User.create(
													name: Faker::Name.name,
													email: Faker::Internet.email,
													password: 'password',
													type: 'RegularUser',
													profile_picture: File.open("#{Rails.root}/app/assets/images/#{['profile1.png','profile2.png', 'profile3.jpeg'].sample}")
							)
						state = india_states.sample
						city = CS.cities(state, :in).sample
						u.build_address(
												address_line: Faker::Address.street_address,
												street: Faker::Address.street_name,
												landmark: "Near" + Faker::Bank.name + " Bank",
												city: city,
												state: state,
												phone_no: ["9876543456", "9876543678"].sample,
												pincode: ["421201", "456545", "423456", "465434"]	.sample
							).save
}



