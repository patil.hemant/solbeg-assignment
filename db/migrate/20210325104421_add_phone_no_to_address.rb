class AddPhoneNoToAddress < ActiveRecord::Migration[6.0]
  def change
    add_column :addresses, :phone_no, :string
  end
end
