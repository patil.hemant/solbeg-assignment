class CreateAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :addresses do |t|
      t.text :address_line
      t.string :street
      t.string :landmark
      t.string :city
      t.string :state
      t.string :pincode
      t.integer :user_id

      t.timestamps
    end
    add_index :addresses, :user_id
  end
end
